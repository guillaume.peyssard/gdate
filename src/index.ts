class GDate {
  year: number;
  month: number;
  dayOfMonth: number;

  constructor(year: number, month: number, dayOfMonth: number) {
    this.year = Number(year);
    this.month = Number(month);
    this.dayOfMonth = Number(dayOfMonth);
  }

  static today() {
    return GDate.fromDate(new Date());
  }

  static parse(isoDateString: string) {
    const splittedDate = isoDateString.split('-');
    return new GDate(Number(splittedDate[0]), Number(splittedDate[1]), Number(splittedDate[2]));
  }

  static fromDate(date: Date) {
    return new GDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
  }

  static fromGDate(date: GDate) {
    return new GDate(date.year, date.month, date.dayOfMonth);
  }

  weekDayName(format: 'long' | 'short' | 'narrow' | undefined = 'long') {
    // format can be long, short or numeric
    const options: Intl.DateTimeFormatOptions = { weekday: format, timeZone: 'Europe/London' };
    return this.toDate().toLocaleDateString('fr-FR', options);
  }

  monthName(format: 'long' | 'short' | 'narrow' | undefined = 'long') {
    // format can be long, short or numeric
    const options: Intl.DateTimeFormatOptions = { month: format, timeZone: 'Europe/London' };
    return this.toDate().toLocaleDateString('fr-FR', options);
  }

  toISOString() {
    function pad(num: number, size: number) {
      let numString: string = num.toString();
      while (numString.length < size) numString = '0' + numString;
      return numString;
    }

    return `${this.year}-${pad(this.month, 2)}-${pad(this.dayOfMonth, 2)}`;
  }

  toString(
    weekDayFormat: 'long' | 'short' | 'narrow' | undefined = 'long',
    monthFormat: 'long' | 'short' | 'narrow' | undefined = 'long',
    yearFormat: 'numeric' | '2-digit' | undefined = 'numeric',
  ) {
    const options: Intl.DateTimeFormatOptions = {
      day: 'numeric',
      timeZone: 'Europe/London',
    };

    if (weekDayFormat) {
      options.weekday = weekDayFormat;
    }

    if (monthFormat) {
      options.month = monthFormat;
    }

    if (yearFormat) {
      options.year = yearFormat;
    }

    return this.toDate().toLocaleDateString('fr-FR', options);
  }

  toDate() {
    return new Date(Date.UTC(this.year, this.month - 1, this.dayOfMonth, 0, 0, 0));
  }

  getWeekDayIndex() {
    /* 0 for monday */

    let dayOfWeekIndex = this.toDate().getUTCDay() - 1;

    if (dayOfWeekIndex === -1) {
      dayOfWeekIndex = 6;
    }

    return dayOfWeekIndex;
  }

  addDays(nbDaysToAdd: number) {
    const date = this.toDate();
    date.setUTCDate(date.getUTCDate() + nbDaysToAdd);
    const res = new GDate(date.getUTCFullYear(), date.getUTCMonth() + 1, date.getUTCDate());
    return res;
  }

  toWeekDayOfSameWeek(weekDayIndex: number) {
    return this.addDays(weekDayIndex - this.getWeekDayIndex());
  }

  toMondayOfPreviousWeek() {
    return this.toMondayOfSameWeek().addDays(-7);
  }

  toTuesdayOfPreviousWeek() {
    return this.toMondayOfSameWeek().addDays(-6);
  }

  toWednesdayOfPreviousWeek() {
    return this.toMondayOfSameWeek().addDays(-5);
  }

  toThursdayOfPreviousWeek() {
    return this.toMondayOfSameWeek().addDays(-4);
  }

  toFridayOfPreviousWeek() {
    return this.toMondayOfSameWeek().addDays(-3);
  }

  toSaturdayOfPreviousWeek() {
    return this.toMondayOfSameWeek().addDays(-2);
  }

  toSundayOfPreviousWeek() {
    return this.toMondayOfSameWeek().addDays(-1);
  }

  toMondayOfSameWeek() {
    return this.toWeekDayOfSameWeek(0);
  }

  toTuesdayOfSameWeek() {
    return this.toWeekDayOfSameWeek(1);
  }

  toWednesdayOfSameWeek() {
    return this.toWeekDayOfSameWeek(2);
  }

  toThursdayOfSameWeek() {
    return this.toWeekDayOfSameWeek(3);
  }

  toFridayOfSameWeek() {
    return this.toWeekDayOfSameWeek(4);
  }

  toSaturdayOfSameWeek() {
    return this.toWeekDayOfSameWeek(5);
  }

  toSundayOfSameWeek() {
    return this.toWeekDayOfSameWeek(6);
  }

  toMondayOfNextWeek() {
    return this.toMondayOfSameWeek().addDays(7);
  }

  toTuesdayOfNextWeek() {
    return this.toMondayOfSameWeek().addDays(8);
  }

  toWednesdayOfNextWeek() {
    return this.toMondayOfSameWeek().addDays(9);
  }

  toThursdayOfNextWeek() {
    return this.toMondayOfSameWeek().addDays(10);
  }

  toFridayOfNextWeek() {
    return this.toMondayOfSameWeek().addDays(11);
  }

  toSaturdayOfNextWeek() {
    return this.toMondayOfSameWeek().addDays(12);
  }

  tosundayOfNextWeek() {
    return this.toMondayOfSameWeek().addDays(13);
  }

  toWeekDays() {
    const weekDaysArray = [];

    for (let i = 0; i < 7; i++) {
      weekDaysArray.push(this.toWeekDayOfSameWeek(i));
    }

    return weekDaysArray;
  }

  toWeekNumber() {
    const d: any = new Date(Date.UTC(this.year, this.month - 1, this.dayOfMonth));
    const dayNum = d.getUTCDay() || 7;
    d.setUTCDate(d.getUTCDate() + 4 - dayNum);
    const yearStart: any = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    return Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
  }

  isBefore(date: GDate) {
    return (
      this.year < date.year ||
      (this.year === date.year && this.month < date.month) ||
      (this.year === date.year && this.month === date.month && this.dayOfMonth < date.dayOfMonth)
    );
  }

  isBeforeOrEqual(date: GDate) {
    return this.isBefore(date) || this.isEqual(date);
  }

  isAfter(date: GDate) {
    return (
      this.year > date.year ||
      (this.year === date.year && this.month > date.month) ||
      (this.year === date.year && this.month === date.month && this.dayOfMonth > date.dayOfMonth)
    );
  }

  isAfterOrEqual(date: GDate) {
    return this.isAfter(date) || this.isEqual(date);
  }

  isEqual(date: GDate) {
    return this.year === date.year && this.month === date.month && this.dayOfMonth === date.dayOfMonth;
  }

  isInThePast() {
    return this.isBefore(GDate.today());
  }

  isInThePastOrToday() {
    return this.isInThePast() || this.isToday();
  }

  isInTheFutur() {
    return this.isAfter(GDate.today());
  }

  isInTheFuturOrToday() {
    return this.isInTheFutur() || this.isToday();
  }

  isToday() {
    return this.isEqual(GDate.today());
  }
}

export { GDate };
